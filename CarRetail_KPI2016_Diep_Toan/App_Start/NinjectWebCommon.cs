using System.Data.Entity;
using CarRetail.Data;
using CarRetail.Data.Infrastructure;
using CarRetail.Data.Membership;
using CarRetail.Data.Repositories;
using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CarRetail_KPI2016_Diep_Toan.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(CarRetail_KPI2016_Diep_Toan.App_Start.NinjectWebCommon), "Stop")]
namespace CarRetail_KPI2016_Diep_Toan.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }


        public static void Stop() => Bootstrapper.ShutDown();


        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<DbContext>().To<CarRetailContext>().InSingletonScope();
            kernel.Bind<IDbFactory>().To<DbFactory>().InSingletonScope();
            kernel.Bind(typeof(IEntityBaseRepository<>)).To(typeof(EntityBaseRepository<>)).InSingletonScope();

            kernel.Bind<MembershipService>().ToSelf().InSingletonScope();
        }
    }
}