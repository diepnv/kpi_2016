﻿using System.Net.Http;
using CarRetail.Data.Membership;
using CarRetail.Data.Repositories;
using CarRetail.Entities;

namespace CarRetail_KPI2016_Diep_Toan.Infrastructure.Extensions
{
    public static class RequestMessageExtensions
    {
        internal static MembershipService GetMembershipService(this HttpRequestMessage request) => request.GetService<MembershipService>();

        internal static IEntityBaseRepository<T> GetDataRepository<T>(this HttpRequestMessage request) where T : class, IEntityBase, new() => request.GetService<IEntityBaseRepository<T>>();

        private static TService GetService<TService>(this HttpRequestMessage request)
        {
            var dependencyScope = request.GetDependencyScope();
            var service = (TService)dependencyScope.GetService(typeof(TService));

            return service;
        }
    }
}