﻿using System.Security.Principal;
using CarRetail.Entities;

namespace CarRetail.Data.Membership
{
    public class MembershipContext
    {
        public IPrincipal Principal { get; set; }
        public User User { get; set; }

        public bool IsValid() => Principal != null;
    }
}