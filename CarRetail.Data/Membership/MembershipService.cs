﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using CarRetail.Data.Repositories;
using CarRetail.Entities;
using CarRetail.Data.Extensions;

namespace CarRetail.Data.Membership
{
    public class MembershipService
    {
        #region Variables

        private readonly IEntityBaseRepository<User> _userRepository;
        private readonly IEntityBaseRepository<Role> _roleRepository;
        private readonly IEntityBaseRepository<UserRole> _userRoleRepository;
        private readonly EncryptionService _encryptionService;

        #endregion Variables

        public MembershipService(IEntityBaseRepository<User> userRepository, IEntityBaseRepository<Role> roleRepository, IEntityBaseRepository<UserRole> userRoleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _encryptionService = new EncryptionService();
        }

        #region IMembershipService Implementation

        public MembershipContext ValidateUser(string username, string password)
        {
            var membershipCtx = new MembershipContext();

            var user = _userRepository.GetSingleByUsername(username);
            if (user == null || !IsUserValid(user, password))
                return membershipCtx;

            var userRoles = GetUserRoles(user.Username);
            membershipCtx.User = user;

            membershipCtx.Principal = new GenericPrincipal(new GenericIdentity(user.Username), userRoles.Select(x => x.Name).ToArray());

            return membershipCtx;
        }

        public User CreateUser(string username, string email, string password, int[] roles)
        {
            var existingUser = _userRepository.GetSingleByUsername(username);

            if (existingUser != null)
            {
                throw new Exception("Username is already in use");
            }

            var passwordSalt = _encryptionService.CreateSalt();

            var user = new User
            {
                Username = username,
                Salt = passwordSalt,
                Email = email,
                IsLocked = false,
                HashedPassword = _encryptionService.EncryptPassword(password, passwordSalt),
                DateCreated = DateTime.Now
            };

            _userRepository.Add(user);

            _userRepository.Commit();

            if (roles != null || roles.Length > 0)
            {
                foreach (var role in roles)
                {
                    AddUserToRole(user, role);
                }
            }

            _roleRepository.Commit();

            return user;
        }

        public User GetUser(int userId) => _userRepository.GetSingle(userId);

        public List<Role> GetUserRoles(string username)
        {
            var result = new List<Role>();

            var existingUser = _userRepository.GetSingleByUsername(username);

            if (existingUser == null)
                return result;

            result.AddRange(existingUser.UserRoles.Select(userRole => userRole.Role));

            return result.Distinct().ToList();
        }

        #endregion IMembershipService Implementation

        #region Helper methods

        private void AddUserToRole(User user, int roleId)
        {
            var role = _roleRepository.GetSingle(roleId);
            if (role == null)
                throw new ApplicationException("Role doesn't exist.");

            var userRole = new UserRole()
            {
                RoleId = role.Id,
                UserId = user.Id
            };
            _userRoleRepository.Add(userRole);
        }

        private bool IsPasswordValid(User user, string password) => string.Equals(_encryptionService.EncryptPassword(password, user.Salt), user.HashedPassword);

        private bool IsUserValid(User user, string password)
        {
            if (IsPasswordValid(user, password))
            {
                return !user.IsLocked;
            }

            return false;
        }

        #endregion Helper methods
    }
}