﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CarRetail.Entities;

namespace CarRetail.Data
{
    public class CarRetailContext : DbContext
    {
        public CarRetailContext()
            : base("CarRetail")
        {
            Database.SetInitializer<CarRetailContext>(null);
        }

        #region Entity Sets

        public IDbSet<User> UserSet { get; set; }
        public IDbSet<Role> RoleSet { get; set; }
        public IDbSet<UserRole> UserRoleSet { get; set; }
        public IDbSet<Customer> CustomerSet { get; set; }
        public IDbSet<Car> CarSet { get; set; }
        public IDbSet<Branch> BranchSet { get; set; }
        public IDbSet<Rental> RentalSet { get; set; }

        #endregion Entity Sets

        public virtual void Commit() => SaveChanges();

        protected override void OnModelCreating(DbModelBuilder modelBuilder) => modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
    }
}