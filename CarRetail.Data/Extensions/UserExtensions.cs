﻿using CarRetail.Data.Repositories;
using CarRetail.Entities;
using System.Linq;

namespace CarRetail.Data.Extensions
{
    public static class UserExtensions
    {
        public static User GetSingleByUsername(this IEntityBaseRepository<User> userRepository, string username) => userRepository.GetAll().FirstOrDefault(x => x.Username == username);
    }
}