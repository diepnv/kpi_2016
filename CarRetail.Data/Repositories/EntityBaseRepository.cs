﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using CarRetail.Data.Infrastructure;
using CarRetail.Entities;

namespace CarRetail.Data.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class, IEntityBase, new()
    {
        private CarRetailContext _dataContext;

        protected IDbFactory DbFactory { get; }

        protected CarRetailContext DbContext => _dataContext ?? (_dataContext = DbFactory.Init());

        public EntityBaseRepository(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public virtual IQueryable<T> GetAll() => DbContext.Set<T>();

        public virtual IQueryable<T> All => GetAll();

        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = DbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public T GetSingle(int id) => GetAll().FirstOrDefault(x => x.Id == id);

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate) => DbContext.Set<T>().Where(predicate);

        public virtual void Add(T entity) => DbContext.Set<T>().Add(entity);

        public virtual void Edit(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public void Commit() => DbContext.Commit();
    }
}