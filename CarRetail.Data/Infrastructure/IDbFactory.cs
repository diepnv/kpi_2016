﻿using System;

namespace CarRetail.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        CarRetailContext Init();
    }
}