﻿namespace CarRetail.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private CarRetailContext _dbContext;

        public CarRetailContext Init() => _dbContext ?? (_dbContext = new CarRetailContext());

        protected override void DisposeCore() => _dbContext?.Dispose();
    }
}