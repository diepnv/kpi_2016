namespace CarRetail.Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<CarRetailContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}