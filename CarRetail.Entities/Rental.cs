﻿using System;

namespace CarRetail.Entities
{
    public class Rental : EntityBase
    {
        public int CustomerId { get; set; }
        public int CarId { get; set; }
        public DateTime RentalDate { get; set; }
        public DateTime? ReturnedDate { get; set; }

        public string Status { get; set; }
        public virtual Car Car { get; set; }
        public virtual Customer Customer { get; set; }
    }
}