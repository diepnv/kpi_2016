﻿using System.Collections.Generic;

namespace CarRetail.Entities
{
    public class Branch : EntityBase
    {
        public string Name { get; set; }
        public virtual List<Car> Cars { get; set; }
    }
}