﻿using System;
using System.Collections.Generic;

namespace CarRetail.Entities
{
    public class Customer : EntityBase
    {
        public Customer()
        {
            RentalHistory = new List<Rental>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string IdentityCard { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Mobile { get; set; }

        public virtual List<Rental> RentalHistory { get; set; }
    }
}