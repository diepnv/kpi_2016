﻿using System.ComponentModel.DataAnnotations;

namespace CarRetail.Entities
{
    public abstract class EntityBase : IEntityBase
    {
        [Key]
        public int Id { get; set; }
    }

    public interface IEntityBase
    {
        int Id { get; set; }
    }
}