﻿using System;
using System.Collections.Generic;

namespace CarRetail.Entities
{
    public class User : EntityBase
    {
        public User()
        {
            UserRoles = new List<UserRole>();
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        public bool IsLocked { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}