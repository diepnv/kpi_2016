﻿namespace CarRetail.Entities
{
    public class Car : EntityBase
    {
        public string Color { get; set; }
        public string Name { get; set; }
        public string CarNumber { get; set; }
        public int BranchId { get; set; }
        public virtual Branch Branch { get; set; }
    }
}